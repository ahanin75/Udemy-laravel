<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::middleware(['admin'])->group(function ()
{
    Route::resource('admin/category', 'CategoryController', ['except' => [
        'show'
    ]]);

    Route::resource('admin/product', 'ProductController', ['except' => [
        'show'
    ]]);

    Route::get('admin/comment','CommentController@index');
    Route::delete('admin/comment/{id}','CommentController@destroy');
    Route::post('admin/set_status','CommentController@set_status');
    Route::post('admin/comment/create','CommentController@create');
    Route::any('admin','AdminController@index');
    Route::resource('admin/discounts', 'DiscountsController', ['except' => [
        'show'
    ]]);

    Route::get('admin/statistics','AdminController@statistics');

    Route::get('admin/setting','AdminController@setting_form');
    Route::post('admin/setting','AdminController@setting');

    Route::get('admin/order','OrderController@index');
    Route::get('admin/order/{id}','OrderController@show');
});




Route::post('cart','SiteController@add_cart');
Route::post('change_number','SiteController@change_number');
Route::post('del_cart','SiteController@del_cart');

//Auth::routes();

Route::get('logout','Auth\LoginController@logout');
Route::get('login','Auth\LoginController@showLoginForm');
Route::post('login','Auth\LoginController@login')->name('login');
Route::get('register','Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register','Auth\RegisterController@register');

Route::get('password/reset','Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email','Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}','Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset','Auth\ResetPasswordController@reset');

Route::get('/home', 'HomeController@index')->name('home');
Route::post('add_comment','SiteController@add_comment');
Route::get('admin/login','Auth\LoginController@admin_login');
Route::get('captcha',function ()
{
    $Captcha=new \App\lib\Captcha();
    $Captcha->create();
});

Route::post('set_discount','SiteController@set_discount');
Route::post('user/add_order','UserController@add_order');

Route::middleware(['statistics'])->group(function()
{
    Route::get('/','SiteController@index');
    Route::get('cart','SiteController@cart');
    Route::get('category','SiteController@category');
    Route::get('category/{cat1}','SiteController@cat1');
    Route::get('category/{cat1}/{cat2}','SiteController@cat2');
    Route::get('{title}','SiteController@show');
});