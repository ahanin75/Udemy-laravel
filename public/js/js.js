function del_row(id,url,token)
{

    var route=url+"/";
    if (!confirm("آیا از حذف این رکورد اطمینان دارید !"))
        return false;

    var form = document.createElement("form");
    form.setAttribute("method", "POST");
    form.setAttribute("action",route+id);
    var hiddenField1 = document.createElement("input");
    hiddenField1.setAttribute("name", "_method");
    hiddenField1.setAttribute("value",'DELETE');
    form.appendChild(hiddenField1);
    var hiddenField2 = document.createElement("input");
    hiddenField2.setAttribute("name", "_token");
    hiddenField2.setAttribute("value",token);
    form.appendChild(hiddenField2);
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}
$("#menu li").click(function () {

    var id=this.id;
    var a="#"+id+ " a";
    $("#menu li ul").hide();
    $("#menu li a").removeClass('menu_li_back');
    $(a).addClass('menu_li_back');
    $('ul',this).show();

    sessionStorage.setItem('menu_id',id);
});

function  show_menu()
{
    if(sessionStorage.getItem('menu_id'))
    {
        var id=sessionStorage.getItem('menu_id');
        var a="#"+id+ " a";
        $(a).addClass('menu_li_back');
        var s="#"+id+ ' ul';
        $(s).show();
    }
}
show_menu();