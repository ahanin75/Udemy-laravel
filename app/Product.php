<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
class Product extends Model
{
    protected $table='product';
    protected $fillable = ['title','title_url','text','view','show','price','time',
    'order_number','links','tag','img','download_tiems','download_number','download_size'];
    public $timestamps = false;
    public static function search($data)
    {
        $product=Product::orderBy('id','desc');
        $string='';
        if(sizeof($data)>0)
        {
            if(array_key_exists('title',$data) && array_key_exists('show',$data))
            {
                $product=$product->where('title','like','%'.$data['title'].'%')
                ->where('show',$data['show']);

                $string='?title='.$data['title'].'&show='.$data['show'];
            }
        }
        $product=$product->paginate(10);

        if(!empty($string))
        {
            $product->withPath($string);
        }
        return $product;
    }
}