<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $array=[
            'fname'=>'required',
            'lname'=>'required',
            'email'=>'required|email',
            'mobile'=>'required',
        ];

        if($this->zip_code)
        {
            $array['zip_code']='integer';
        }
        return $array;
    }
    public function attributes()
    {
        return [
            'fname'=>'نام',
            'lname'=>'نام خانوادگی',
            'email'=>'ایمیل',
            'mobile'=>'شماره موبایل',
            'zip_code'=>'کد پستی'
        ];
    }
}
