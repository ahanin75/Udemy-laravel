<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|unique:product,title,'.$this->product.'',
            'price'=>'integer',
            'file'=>'image|max:1024'
        ];
    }
    public function attributes()
    {
       return [
           'title'=>'عنوان محصول',
           'price'=>'هزینه محصول '
       ];
    }
}
