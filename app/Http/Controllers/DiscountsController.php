<?php
namespace App\Http\Controllers;
use App\Category;
use App\Discounts;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\DiscountRequest;
use Illuminate\Http\Request;
use DB;
use View;
use Validator;
class DiscountsController extends Controller
{
    public function index()
    {
        $discounts=Discounts::orderBy('id','DESC')->paginate(10);
        return View('discounts.index',['discounts'=>$discounts]);
    }
    public function create()
    {
       return View('discounts.create');
    }
    public function store(DiscountRequest $request)
    {
        $discounts=new Discounts($request->all());
        $discounts->save();
        $url='admin/discounts/'.$discounts->id.'/edit';
        return redirect($url);
    }
    public function edit($id)
    {
       $discount=Discounts::findOrfail($id);
       return View('discounts.update',['model'=>$discount]);
    }
    public function update(DiscountRequest $request,$id)
    {
        $model=Discounts::findOrfail($id);
        $model->update($request->all());
        $url='admin/discounts/'.$model->id.'/edit';
        return redirect($url);
    }
    public function destroy($id)
    {
        $model=Discounts::findOrfail($id);
        $model->delete();
        return redirect()->back();
    }

}