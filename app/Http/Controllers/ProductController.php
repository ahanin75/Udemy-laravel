<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\ProductRequest;
use App\Product;
use Illuminate\Http\Request;
use DB;
class ProductController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $product=Product::search($request->all());
        return view('product.index',['product'=>$product]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cat=Category::where('parent_id',0)->get();
        return View('product.create',['cat'=>$cat]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $product=new Product($request->all());
        $product->view=0;
        $product->time=time();
        $product->order_number=0;
        $url=str_replace('-','',$product->title);
        $url=str_replace('/','',$url);
        $product->title_url=preg_replace('/\s+/','-',$url);
        if($request->hasFile('file'))
        {
            $file_name=time().'.'.$request->file('file')->getClientOriginalExtension();
            if($request->file('file')->move('upload',$file_name))
            {
                $product->img='upload/'.$file_name;
            }
        }

        $cat=$request->get('product_cat');
        if($product->save())
        {
            if(is_array($cat))
            {
                foreach ($cat as $key=>$value)
                {
                    DB::table('product_category')->insert(['product_id'=>$product->id,'category_id'=>$value]);
                }
            }
            $url='admin/product/'.$product->id.'/edit';
            return redirect($url);
        }
        else
        {
            return redirect()->back();
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model=Product::findOrfail($id);
        $cat=Category::where('parent_id',0)->get();
        $product_cat=DB::table('product_category')->where('product_id',$id)->pluck('id','category_id')->toArray();
         return View('product.update',['cat'=>$cat,'model'=>$model,'product_cat'=>$product_cat]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id)
    {
        $product=Product::findOrfail($id);
        $url=str_replace('-','',$request->title);
        $url=str_replace('/','',$url);
        $product->title_url=preg_replace('/\s+/','-',$url);
        if($request->hasFile('file'))
        {
            $file_name=time().'.'.$request->file('file')->getClientOriginalExtension();
            if($request->file('file')->move('upload',$file_name))
            {
                $product->img='upload/'.$file_name;
            }
        }
        $cat=$request->get('product_cat');
        if($product->update($request->all()))
        {
            if(is_array($cat))
            {
                DB::table('product_category')->where('product_id',$product->id)->delete();
                foreach ($cat as $key=>$value)
                {
                    DB::table('product_category')->insert(['product_id'=>$product->id,'category_id'=>$value]);
                }
            }
        }
        $url='admin/product/'.$product->id.'/edit';
        return redirect($url);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model=Product::findOrfail($id);
        $model->delete();
        DB::table('product_category')->where('product_id',$id)->delete();
        return redirect()->back();
    }
}
