<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Order;
use Illuminate\Http\Request;
use Session;
use Auth;
class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function add_order(OrderRequest $request)
    {
        $Order=new Order($request->all());
        $Order->time=time();
        $Order->date=jdate()->format('Y-n-d');
        $product_id='';
        $number_product='';
        foreach(Session::get('cart') as $key=>$value)
        {
            $product_id.=$key.',';
            $number_product.=$value.',';
        }


        $Order->product_id=$product_id;
        $Order->number_product=$number_product;
        $Order->payment_status='no';
        $Order->RefId='a';
        $Order->order_read='no';
        $Order->total_price=Session::get('total_price');
        $Order->price=Session::get('cart_price');
        $Order->user_id=Auth::user()->id;
        $Order->save();
    }
}
