<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use URL;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected function redirectTo()
    {
        $role=Auth::user()->role;
        if($role=='admin')
        {
          return 'admin';
        }
        else
        {
            return '/';
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
            'captcha'=>'required|captcha'
        ],[],[
            'email'=>'ایمیل',
            'password'=>'کلمه عبور',
            'captcha'=>'تصویر امنیتی',
            'username'=>'نام کاربری'
        ]);
    }
    public function admin_login()
    {
       return view('admin.login');
    }

    public function username()
    {
        $back_url=URL::previous();
        if($back_url==url('admin/login'))
        {
            return 'username';
        }
        else
        {
            return 'email';
        }
    }

    protected function credentials(Request $request)
    {
        $array=$request->only($this->username(), 'password');

        $back_url=URL::previous();
        if($back_url==url('admin/login'))
        {
            $array['role']='admin';
        }
        else
        {
            $array['role']='user';
        }

        return $array;
    }
}
