<?php
namespace App\Http\Controllers;
use App\Category;
use App\Comment;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\CommentRequest;
use App\Product;
use App\User;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;
use View;
use Validator;
use Illuminate\Support\Collection;
use Auth;
use Session;
use App;
use Hash;
class SiteController extends Controller
{
    public function index(Request $request)
    {
        $product=Product::where('show',1)->orderBy('id','DESC')->paginate(10);
        return View('site/index',['product'=>$product]);
    }
    public function show($title)
    {
       $product=Product::where(['title_url'=>$title,'show'=>1])->firstOrFail();
       $product->increment('view');
       $comment=Comment::where(['product_id'=>$product->id,'status'=>1,'parent_id'=>0])->orderBy('id','DESC')->paginate(10);
       return view('site.show',['product'=>$product,'comment'=>$comment]);
    }
    public function add_cart(Request $request)
    {
        $product_id=$request->get('product_id');
        $product=Product::findOrFail($product_id);
        if(!empty($product->price))
        {
            $array=array();
            if(Session::has('cart'))
            {
                $array=Session::get('cart');
                if(array_key_exists($product_id,$array))
                {
                    $array[$product_id]++;
                    Session::put('cart',$array);
                }
                else
                {
                    $array[$product_id]=1;
                    Session::put('cart',$array);
                }
            }
            else
            {
                $array[$product_id]=1;
                Session::put('cart',$array);
            }
        }


        return View('site/cart');

    }
    public function cart(Request $request)
    {
        return View('site/cart');
    }
    public function change_number(Request $request)
    {
        if($request->ajax())
        {
            $product_id=$request->get('product_id');
            $number=$request->get('number');
            settype($number,'integer');
            if(is_integer($number) && $number>0)
            {
                if(Session::has('cart'))
                {
                    $array=Session::get('cart');
                    if(array_key_exists($product_id,$array))
                    {
                        $array[$product_id]=$number;

                        Session::put('cart',$array);
                    }
                }

            }

            return View('site/ajax_cart');
        }
    }
    public function del_cart(Request $request)
    {
        $product_id=$request->get('product_id');
        if($request->ajax())
        {
            if(Session::has('cart'))
            {
                $array=Session::get('cart');
                if(array_key_exists($product_id,$array))
                {
                    unset($array[$product_id]);
                    if(empty($array))
                    {
                        Session::forget('cart');
                    }
                    else
                    {
                        Session::put('cart',$array);
                    }
                }
            }
            return View('site/ajax_cart');
        }
    }
    public function add_comment(CommentRequest $request)
    {
        $msg='خطا در ثبت نظر';
        $product_id=$request->get('product_id');
        $Product=Product::findOrFail($product_id);
        $comment=new Comment($request->all());
        $comment->status ='0';
        if($comment->save())
        {
            $msg='نظر با موفقیت ثبت شد و بعد از تایید مدیریت نمایش داده می شود';
        }

        return redirect()->back()->with('msg',$msg);

    }
    public function category()
    {
        $Category=Category::with('getChild')->where('parent_id',0)->get();
        return View('site.category',['category'=>$Category]);
    }
    public function cat1($cat1)
    {
        $category=Category::where('cat_ename',$cat1)->firstOrFail();
        $cats=DB::table('product_category')->orderBy('product_id','DESC')->where('category_id',$category->id)->paginate(10);
        $product_id=array();
        foreach ($cats as $key=>$value)
        {
            $product_id[$key]=$value->product_id;
        }
        $product=Product::where('show',1)
            ->whereIn('id',$product_id)
            ->orderBy('id','DESC')->get();
        return View('site/product_cat',['product'=>$product,'cats'=>$cats]);
    }
    public function cat2($cat1,$cat2)
    {
        $category1=Category::where('cat_ename',$cat1)->firstOrFail();
        $category2=Category::where('cat_ename',$cat2)->firstOrFail();
        if($category2->parent_id==$category1->id)
        {

            $cats=DB::table('product_category')->orderBy('product_id','DESC')->where('category_id',$category2->id)->paginate(10);
            $product_id=array();
            foreach ($cats as $key=>$value)
            {
                $product_id[$key]=$value->product_id;
            }
            $product=Product::where('show',1)
                ->whereIn('id',$product_id)
                ->orderBy('id','DESC')->get();
            return View('site/product_cat',['product'=>$product,'cats'=>$cats]);

        }
        else
        {
            abort(404);
        }
    }
    public function set_discount(Request $request)
    {
        $error='';
        $discount=$request->get('discount');
        $row=App\Discounts::where(['discount_name'=>$discount])->first();
        if($row)
        {
            Session::put('discount',$row->discount_value);
            $error='کد تخفیف وارد شده صحیح می باشد';
        }
        else
        {
            $error='کد تخفیف وارد شده اشتباه می باشد';
        }
        return View('site/ajax_cart',['error'=>$error]);
    }
}