<?php
namespace App\Http\Controllers;
use App\Category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;
use DB;
use View;
use Validator;
class CategoryController extends Controller
{
    public function index()
    {
        $category=Category::orderBy('id','DESC')->paginate(20);
        return View('category.index',['category'=>$category]);

    }
    public function create()
    {
       $array=[0=>'انتخاب سر دسته'];
       $cat_list=Category::where('parent_id',0)->pluck('cat_name','id')->toArray();
       $cat_list=$array+$cat_list;
       return View('category.create',['cat_list'=>$cat_list]);
    }
    public function store(CategoryRequest $request)
    {
        $category=new Category($request->all());
        $category->save();
        $url='admin/category/'.$category->id.'/edit';
        return redirect($url);
    }
    public function edit($id)
    {
       $model=Category::findOrfail($id);
       $array=[0=>'انتخاب سر دسته'];
       $cat_list=Category::where('parent_id',0)->pluck('cat_name','id')->toArray();
       $cat_list=$array+$cat_list;
       return View('category.update',['cat_list'=>$cat_list,'model'=>$model]);
    }
    public function update(CategoryRequest $request,$id)
    {
        $model=Category::findOrfail($id);
        $model->update($request->all());
        $url='admin/category/'.$model->id.'/edit';
        return redirect($url);
    }
    public function destroy($id)
    {
        $model=Category::findOrfail($id);
        $model->delete();
        return redirect()->back();
    }

}