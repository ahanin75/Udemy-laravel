<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $order=Order::search($request->all());

        return View('order.index',['order'=>$order,
            'email'=>$request->get('email'),'order_number'=>$request->get('order_number')]);
    }
    public function show($id)
    {
        $order=Order::findOrFail($id);
        return View('order.show',['order'=>$order]);
    }
}
