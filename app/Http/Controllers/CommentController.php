<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index()
    {
        $comment=Comment::with('product')->orderBy('id','DESC')->paginate(10);

        return View('comment.index',['comment'=>$comment]);
    }
    public function destroy($id)
    {
        $Comment=Comment::findOrfail($id);
        $Comment->delete();
        return redirect()->back();
    }
    public function set_status(Request $request)
    {
        if($request->ajax())
        {
            $product_id=$request->get('product_id');
            $Comment=Comment::find($product_id);
            if($Comment)
            {
                if($Comment->status==0)
                {
                    $Comment->status=1;
                    $Comment->update();
                    return 1;
                }
                else
                {
                    $Comment->status=0;
                    $Comment->update();
                    return 2;
                }

            }
        }

    }
    public function create(Request $request)
    {
        if(!empty($request->get('content')))
        {
            $Comment=new Comment($request->all());
            $Comment->name='مدیر';
            $Comment->email='ali.sedighi.tu@gmail.com';
            $Comment->status=1;
            $Comment->save();


        }
       return redirect()->back();
    }
}
