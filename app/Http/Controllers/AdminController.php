<?php
namespace App\Http\Controllers;
use App\Category;
use App\Comment;
use App\Http\Requests\CategoryRequest;
use App\Jobs\CreatePost;
use App\Jobs\CreatePost2;
use App\Mail\Order;
use App\Notifications\SendEmail;
use App\Post;
use App\Product;
use App\User;
use App\Users;
use Carbon\Carbon;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use DB;
use Notification;
use View;
use Validator;
use Illuminate\Support\Collection;
use Auth;
use Illuminate\Support\Facades\Cache;
use Mail;
use Storage;
class AdminController extends Controller
{
    public function index(Request $request)
    {

    }
    public function statistics()
    {
        $total_view=array();
        $view=array();
        $date=array();
        for($i=29;$i>=0;$i--)
        {
            if($i==0)
            {
                $string='today';
                $time=strtotime($string);
                $year=jdate($time)->format('Y');
                $month=jdate($time)->format('n');
                $day=jdate($time)->format('d');
                $row=DB::table('statistics')->where(['year'=>$year,'month'=>$month,'day'=>$day])->first();
                $date[$i]=$year.'-'.$month.'-'.$day;
                if($row)
                {
                    $view[$i]=$row->view;
                    $total_view[$i]=$row->total_view;
                }
                else
                {
                    $view[$i]=0;
                    $total_view[$i]=0;
                }
            }
            else
            {
                $string='-'.$i.' day';
                $time=strtotime($string);
                $year=jdate($time)->format('Y');
                $month=jdate($time)->format('n');
                $day=jdate($time)->format('d');
                $row=DB::table('statistics')->where(['year'=>$year,'month'=>$month,'day'=>$day])->first();
                $date[$i]=$year.'-'.$month.'-'.$day;
                if($row)
                {
                    $view[$i]=$row->view;
                    $total_view[$i]=$row->total_view;
                }
                else
                {
                    $view[$i]=0;
                    $total_view[$i]=0;
                }
            }
        }


        return View('admin.statistics',
            [
                'view'=>$view,
                'total_view'=>$total_view,
                'date'=>$date
            ]);

    }
    public function setting_form()
    {
        $data=array();
        $option_name=array('terminalid','username','password');
        foreach ($option_name as $key=>$value)
        {
            $row=DB::table('settings')->where(['option_name'=>$value])->first();
            if($row)
            {
                $data[$value]=$row->option_value;
            }
            else{
                $data[$value]='';
            }
        }

        return View('admin.setting_form',['data'=>$data]);
    }
    public function setting(Request $request)
    {
        $data=array();
        foreach ($request->all() as $key=>$value)
        {
            if($key!='_token')
            {
                $row=DB::table('settings')->where(['option_name'=>$key])->first();
                if($row)
                {
                    DB::table('settings')->where(['option_name'=>$key])->update(['option_value'=>$value]);
                    $data[$key]=$value;
                }
                else
                {
                    DB::table('settings')->insert(['option_value'=>$value,'option_name'=>$key]);
                    $data[$key]=$value;
                }
            }
        }
        return View('admin.setting_form',['data'=>$data]);
    }
}