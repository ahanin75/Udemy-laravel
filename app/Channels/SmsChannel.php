<?php
namespace  App\Channels;
use Illuminate\Notifications\Notification;
use DB;
class  SmsChannel
{
    public function send($notifiable,Notification $notification)
    {
        $data=$notification->toSms($notifiable);
        DB::table('post')->insert($data);
    }
}