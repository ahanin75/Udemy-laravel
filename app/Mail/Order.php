<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Order extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public  $name='idehpardazanjavan.com';

    public function __construct()
    {
        //
    }

    public  $tries=2;

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('سفارش شما')->markdown('email.order')
            ->attach('upload/1504155911.png',
                [
                    'as'=>'image.png',
                ]);
    }
}
