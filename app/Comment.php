<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
class Comment extends Model
{
    protected $table = 'comment';
    protected $fillable = ['name','email','content','parent_id','created_at','updated_at',
        'product_id','status'];
    //public $timestamps = false;

    public $dateFormat='U';

    public function get_child()
    {
        return $this->hasMany('App\Comment','parent_id','id');
    }
    public function get_parent()
    {
        return $this->hasOne('App\Comment','id','parent_id')->withDefault();
    }
    public function product()
    {
        return $this->hasOne('App\Product','id','product_id')->withDefault();
    }
}