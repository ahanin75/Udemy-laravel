<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
class Post extends Model
{
    protected $table = 'post';
    protected $fillable = ['title','user_id','body'];
    public $timestamps = false;

    public function create_post()
    {
        return  'create_post';
    }
}