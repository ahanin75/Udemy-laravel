<?php

namespace App\Providers;


use App\User;
use Illuminate\Support\ServiceProvider;
use Validator;
use Session;
use Route;
use Illuminate\Database\Eloquent\Relations\Relation;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('captcha',function ($attribute,$value,$parameters){

            if($value==Session::get('Captcha'))
            {
                return true;
            }
            else
            {
                return false;
            }

        });
        Relation::morphMap([
            'posts' => 'App\Post',
            'videos' => 'App\Video',
        ]);


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
