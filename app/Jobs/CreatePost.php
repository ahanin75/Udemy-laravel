<?php

namespace App\Jobs;

use App\Post;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CreatePost implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public  $tries=4;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $Post=new Post();
        $Post->title='a';
        $Post->body='b';
        $Post->users_id=1;
        $Post->save();
    }

    public function failed(\Exception $exception)
    {
        $Post=new Post();
        $Post->title='failed_title';
        $Post->body='failed_body';
        $Post->user_id=1;
        $Post->save();
    }
}
