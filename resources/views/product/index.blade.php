@extends('layouts/admin')

@section('header')
    <title>مدیریت محصولات</title>
@endsection

@section('content')

    <div class="box_title">
        <span>مدیریت محصولات</span>
    </div>

    {!! Form::open(['url' => 'admin/product','method'=>'GET']) !!}

    <div class="form-group">
        {{  Form::label('title', 'عنوان محصول : ') }}
        {{ Form::text('title',null,['class' => 'form-control']) }}
        @if($errors->has('title'))
            <span style="color:red;font-size:13px">{{ $errors->first('title') }}</span>
        @endif
    </div>

    <div class="form-group">
        {{  Form::label('show', 'وضعیت محصول : ') }}
        {{ Form::select('show',[1=>'منتشر شده',0=>'پیش نویس'],null,['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{  Form::submit('جست و جو',['class'=>'btn btn-primary']) }}
    </div>

    {!! Form::close() !!}

    <div>
        <a class="btn btn-success" href="{{ url('admin/product/create') }}">
          محصول جدید
        </a>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>ردیف</th>
                <th>تصویر شاخص</th>
                <th>عنوان محصول</th>
                <th>تعداد فروش</th>
                <th>میزان بازدید</th>
                <th>وضعیت</th>
                <th>عملیات</th>
            </tr>
            </thead>
            <?php $i=1; ?>
            @foreach($product as $key=>$value)

                <tr>
                    <td>{{ $i }}</td>
                    <td>
                        @if(!empty($value->img))
                        <img src="{{ url('').'/'.$value->img }}" width="150px">
                        @endif
                    </td>
                    <td>{{ $value->title }}</td>
                    <td>{{ $value->order_number }}</td>
                    <td>{{ $value->view }}</td>
                    <td>
                        @if($value->show==1)
                            <a class="btn btn-success">منتشر شده</a>
                        @else
                            <a class="btn btn-success" style="background: red;border:1px solid red">پیش نویس</a>
                        @endif
                    </td>

                    <td>
                        <a style="color:#368bff" href="{{ url('admin/product').'/'.$value->id.'/edit' }}">
                            <span class="fa fa-edit"></span>
                        </a>
                        <a style="cursor:pointer;color:red;padding-right:5px" onclick="del_row('<?= $value->id ?>','<?= url('admin/product') ?>','<?= Session::token() ?>')">
                            <span class="fa fa-remove"></span>
                        </a>
                    </td>
                </tr>
                <?php $i++; ?>
            @endforeach

            @if(sizeof($product)==0)
                <tr>
                    <td colspan="7">نتیجه ای یافت نشد</td>
                </tr>
            @endif
        </table>

        {{ $product->links() }}


    </div>
@endsection
