@extends('layouts/admin')


@section('header')
    <title>افزودن محصول</title>
@endsection

@section('content')

    <div class="box_title">
        <span>افزودن محصول</span>
    </div>

    {!! Form::open(['url' => 'admin/product','files'=>true]) !!}
    <div class="form-group">
        {{  Form::label('title', 'عنوان محصول') }}
        {{ Form::text('title',null,['class' => 'form-control','style'=>'width:75%;']) }}
        @if($errors->has('title'))
            <span style="color:red;font-size:13px">{{ $errors->first('title') }}</span>
        @endif
    </div>

    <div class="form-group">
        {{ Form::textArea('text',null,['class'=>'ckeditor','style'=>'width:90%'])}}
    </div>

    <div class="form-group">
        {{  Form::label('show', 'وضعیت محصول : ') }}
        {{ Form::select('show',[1=>'منتشر شده',0=>'پیش نویس'],null,['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{  Form::label('price', 'هزینه محصول : ') }}
        {{ Form::text('price',null,['class' => 'form-control']) }}
        @if($errors->has('price'))
            <span style="color:red;font-size:13px">{{ $errors->first('price') }}</span>
        @endif
    </div>

    <div class="form-group">
        {{  Form::label('file', 'تصویر محصول : ') }}
        {{ Form::file('file', $attributes = []) }}
        @if($errors->has('file'))
            <span style="color:red;font-size:13px">{{ $errors->first('file') }}</span>
        @endif
    </div>



    <div class="form-group" style="background:#f9f9f9">

        <ul class="list-unstyled">

            @foreach($cat as $key=>$value)

                <li><input type="checkbox" name="product_cat[]" value="{{ $value->id }}"> <span style="padding-right:5px">{{ $value->cat_name }}</span>

                    <ul>
                        @foreach($value->getChild as $key2=>$value2)
                        <li>
                        <input type="checkbox" name="product_cat[]" value="{{ $value2->id }}"> <span style="padding-right:5px">{{ $value2->cat_name }}</span>
                        </li>
                        @endforeach
                    </ul>
                </li>
            @endforeach
        </ul>

    </div>

    <div class="form-group">
        {{  Form::label('download_number', 'تعداد قسمت های محصول : ') }}
        {{ Form::text('download_number',null,['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{  Form::label('download_tiems', 'مدت زمان دوره : ') }}
        {{ Form::text('download_tiems',null,['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{  Form::label('download_size', 'حجم فایل های محصول : ') }}
        {{ Form::text('download_size',null,['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{  Form::label('show', 'لینک های محصول : ') }}
        {{ Form::textArea('links',null,['class'=>'form-control','style'=>'width:75%;text-align:left'])}}
    </div>

    <div class="form-group">
        {{  Form::label('tag', 'برچسب های محصول : ') }}
        {{ Form::textArea('tag',null,['class'=>'form-control','style'=>'width:75%'])}}
    </div>

    <div class="form-group">
        {{  Form::submit('ثبت',['class'=>'btn btn-success']) }}
    </div>

    {!! Form::close() !!}
@endsection

@section('footer')
<script type="text/javascript" src="{{ url('ckeditor/ckeditor.js') }}"></script>
@endsection