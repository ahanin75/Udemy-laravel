@extends('layouts/admin')


@section('header')
<title>ویرایش کد تخفیف</title>
@endsection

@section('content')

    <div class="box_title">
        <span>ویرایش کد تخفیف</span>
    </div>

    {!! Form::model($model,['url' => 'admin/discounts'.'/'.$model->id]) !!}
    {{ method_field('PUT') }}
    <div class="form-group">
        {{  Form::label('discount_name', 'کد تخفیف : ') }}
        {{ Form::text('discount_name',null,['class' => 'form-control']) }}
        @if($errors->has('discount_name'))
            <span style="color:red;font-size:13px">{{ $errors->first('discount_name') }}</span>
        @endif
    </div>


    <div class="form-group">
        {{  Form::label('discount_value ', 'مقدار تخفیف : ') }}
        {{ Form::text('discount_value',null,['class' => 'form-control']) }}
        @if($errors->has('discount_value'))
            <span style="color:red;font-size:13px">{{ $errors->first('discount_value') }}</span>
        @endif
    </div>


    <div class="form-group">
        {{  Form::submit('ویرایش',['class'=>'btn btn-primary']) }}
    </div>
    {!! Form::close() !!}

@endsection