@extends('layouts/admin')

@section('header')
    <title>کد های تخفیف</title>
@endsection

@section('content')

    <div class="box_title">
        <span>کد های تخفیف</span>
    </div>



    <div>
        <a class="btn btn-success" href="{{ url('admin/discounts/create') }}">
ایجاد کد تخفیف
        </a>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>ردیف</th>
                <th>کد تخفیف</th>
                <th>مقدار تخفیف</th>
                <th>عملیات</th>
            </tr>
            </thead>
            <?php $i=1; ?>
            @foreach($discounts as $key=>$value)

                <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $value->discount_name }}</td>
                    <td>
                        {{ $value->discount_value }}
                    </td>
                    <td>
                        <a style="color:#368bff" href="{{ url('admin/discounts').'/'.$value->id.'/edit' }}">
                            <span class="fa fa-edit"></span>
                        </a>
                        <a style="cursor:pointer;color:red;padding-right:5px" onclick="del_row('<?= $value->id ?>','<?= url('admin/discounts') ?>','<?= Session::token() ?>')">
                            <span class="fa fa-remove"></span>
                        </a>
                    </td>
                </tr>
                <?php $i++; ?>
            @endforeach
        </table>

        {{ $discounts->links() }}


    </div>
@endsection
