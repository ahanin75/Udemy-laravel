@extends('layouts/admin')

@section('header')
    <title>مدیریت سفارشات</title>
@endsection

@section('content')


    <div class="box_title">
        <span>مدیریت سفارشات</span>
    </div>


    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>ردیف</th>
            <th>شماره سفارش</th>
            <th>ایمیل</th>
            <th>زمان سفارش</th>
            <th>وضعیت</th>
            <th>عملیات</th>
        </tr>
        </thead>
        <form method="get" id="order_search_form">
        <tr>

            <td></td>
            <td><input type="text" value="{{ $order_number }}" class="form-control search_input" style="width:100%"  name="order_number"></td>
            <td><input type="text" value="{{ $email }}" class="form-control search_input" style="width:100%" name="email"  ></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        </form>
        <?php $i=1; ?>
        @if(sizeof($order)==0)
        <tr>
            <td colspan="6">رکوردی یافت نشد</td>
        </tr>
        @endif
        @foreach($order as $key=>$value)
            <tr>
                <td>{{ $i }}</td>
                <td>
                    @if($value->order_read=='no')
                        <span style="color:red">{{ $value->time }}</span>
                        @else
                        {{ $value->time }}
                    @endif
                </td>
                <td>{{ $value->email }}</td>
                <td>
                    <?php

                       echo jdate($value->time)->format('Y-n-d - H:i:s');
                ?>
                </td>
                <td>
                    @if($value->payment_status=='no')
                        معلق
                        @else
                        پرداخت شده
                    @endif
                </td>
                <td>
                    <a style="color:#368bff" href="{{ url('admin/order').'/'.$value->id }}">
                        <span class="fa fa-eye"></span>
                    </a>
                </td>
            </tr>
            <?php $i++; ?>
        @endforeach
    </table>


    {{ $order->links() }}
@endsection


@section('footer')
<script>


    $('.search_input').on('keydown',function (event)
    {
        if(event.keyCode==13)
        {
             $("#order_search_form").submit();
        }

    });

</script>
@endsection
