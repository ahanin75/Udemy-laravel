@extends('layouts/admin')

@section('header')
    <title>مدیریت سفارشات</title>
@endsection

@section('content')


    <div class="box_title">
        <span>سفارش - {{ $order->id }}</span>
    </div>


    <table class="table table-striped table-bordered detail-view" id="order_table">

        <tr>
            <td style="width:30%">نام </td>
            <td>{{ $order->fname }}</td>
        </tr>

        <tr>
            <td style="width:30%">نام خانوادگی </td>
            <td>{{ $order->lname }}</td>
        </tr>


        <tr>
            <td style="width:30%">ایمیل</td>
            <td>{{ $order->email }}</td>
        </tr>

        <tr>
            <td style="width:30%">شماره موبایل</td>
            <td>{{ $order->mobile }}</td>
        </tr>


        <tr>
            <td style="width:30%">کد پستی</td>
            <td>{{ $order->zip_code }}</td>
        </tr>

        <tr>
            <td style="width:30%">آدرس پستی</td>
            <td>{{ $order->address }}</td>
        </tr>

    </table>


    <div style="width:90%;margin:auto">
        <p style="color:red;padding-top: 20px;padding-bottom: 20px;">محصولات خریداری شده</p>
    </div>

@endsection