@extends('layouts.site')

@section('content')
    <div class="container-fluid">

        <div class="row" style="width:90%;margin:10px auto;background:white;padding-bottom: 20px;">


            <div class="box_title">
                <span>ورود به سایت</span>
            </div>

            <form id="register_form" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}

                <div class="form-group">
                    <label class="control-label" for="users-fname">ایمیل</label>
                    <input id="name" type="text" class="form-control" name="email" value="{{ old('email') }}">
                    <div class="help-block">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label" for="users-fname">کلمه عبور</label>
                    <input id="name" type="password" class="form-control" name="password" >
                    <div class="help-block">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label" for="users-fname">تصویر امنیتی</label>
                    <img src="{{ url('captcha') }}" style="margin-right:60px">
                </div>


                <div class="form-group">
                    <label class="control-label" for="users-fname"></label>
                    <input id="name" type="text" class="form-control" name="captcha" >
                    <div class="help-block">
                        @if ($errors->has('captcha'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('captcha') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">

                    <label class="control-label" for="users-fname">
                        مرا به خاطر بسپار</label>
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>


                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">ورود به سایت</button>
                </div>
            </form>

        </div>

    </div>
@endsection
