@extends('layouts.site')

@section('content')
    <div class="container-fluid">

        <div class="row" style="width:90%;margin:10px auto;background:white;padding-bottom: 20px;">

            <div class="box_title">
                <span>ثبت نام در سایت</span>
            </div>




                <form id="register_form" method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}

                    <div class="form-group">
                        <label class="control-label" for="users-fname">نام</label>
                        <input id="name" type="text" class="form-control" name="fname" value="{{ old('fname') }}">
                        <div class="help-block">
                            @if ($errors->has('fname'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('fname') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label" for="users-fname">نام خانوادگی</label>
                        <input id="name" type="text" class="form-control" name="lname" value="{{ old('lname') }}">
                        <div class="help-block">
                            @if ($errors->has('lname'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('lname') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="users-fname">ایمیل</label>
                        <input id="name" type="text" class="form-control" name="email" value="{{ old('email') }}">
                        <div class="help-block">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label" for="users-fname">کلمه عبور</label>
                        <input id="name" type="password" class="form-control" name="password1">
                        <div class="help-block">
                            @if ($errors->has('password1'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password1') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="users-fname">تکرار کلمه عبور</label>
                        <input id="name" type="password" class="form-control" name="password2">
                        <div class="help-block">
                            @if ($errors->has('password2'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password2') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">ثبت نام در سایت</button>
                    </div>
                </form>


        </div>
    </div>
@endsection
