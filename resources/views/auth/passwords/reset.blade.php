@extends('layouts.site')

@section('content')


    <div class="container-fluid">

        <div class="row" style="width:90%;margin:10px auto;background:white;padding-bottom: 20px;">


            <div class="box_title">
                <span>کلمه عبور جدید</span>
            </div>


            <form id="register_form" method="POST" action="{{ route('password.request') }}">
            {{ csrf_field() }}

                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group">
                    <label class="control-label" for="users-fname">ایمیل</label>
                    <input id="name" type="text" class="form-control" name="email" value="{{ old('email') }}">
                    <div class="help-block">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label" for="users-fname">کلمه عبور</label>
                    <input id="name" type="password" class="form-control" name="password">
                    <div class="help-block">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label" for="users-fname">تکرار کلمه عبور</label>
                    <input id="name" type="password" class="form-control" name="password_confirmation">
                    <div class="help-block">
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">ثبت</button>
                </div>



            </form>
        </div>

    </div>
@endsection
