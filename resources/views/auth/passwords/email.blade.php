@extends('layouts.site')

@section('content')


    <div class="container-fluid">

        <div class="row" style="width:90%;margin:10px auto;background:white;padding-bottom: 20px;">


            <div class="box_title">
                <span>بازیابی کلمه عبور</span>
            </div>


            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <form  id="register_form" method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}


                <div class="form-group">
                    <label class="control-label" for="users-fname">ایمیل</label>
                    <input id="name" type="text" class="form-control" name="email" value="{{ old('email') }}">
                    <div class="help-block">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>


                <div class="form-group">
                    <button type="submit" class="btn btn-primary">کلمه عبور جدید</button>
                </div>

            </form>
        </div>
    </div>
@endsection
