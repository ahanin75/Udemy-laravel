<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ url('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ url('css/bootstrap-rtl.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/admin.css') }}" rel="stylesheet">
    <link href="{{ url('css/font-awesome.css') }}" rel="stylesheet">
    @yield('header')
</head>

<body>

<nav id="w0" class="navbar-inverse navbar-fixed-top navbar" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w0-collapse"><span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span></button><a class="navbar-brand" href="">پنل مدیریت</a>
        </div><div id="w0-collapse" class="collapse navbar-collapse">

        </div></div></nav>
<div class="container-fluid">



    <div class="row">

        <div class="col-sm-3 col-md-2 sidebar">


            <?php

               $comment_count=DB::table('comment')->where('status',0)->count();
               if($comment_count>0)
               {
                    $comment= 'نظرات ('.$comment_count.')';
               }
               else
               {
                   $comment='نظرات';
               }
             ?>
            <ul class="nav nav-sidebar" id="menu">

                <li id="li_1">
                    <a>
                        <span class="fa fa-shopping-cart"></span>
                        <span>محصولات</span>
                    </a>
                    <ul>
                        <li><a href="{{ url('admin/product') }}">مدیریت محصولات</a></li>
                        <li><a href="{{ url('admin/product/create') }}">محصول جدید</a></li>
                        <li><a href="{{ url('admin/category') }}">مدیریت دسته ها</a></li>
                        <li><a href="{{ url('admin/category/create') }}">دسته جدید</a></li>
                    </ul>
                </li>

                <li id="li_2">
                    <a>
                        <span class="fa fa-list"></span>
                        <span>سفارشات</span>
                    </a>
                    <ul>
                        <li><a href="{{ url('admin/order') }}">مدیریت سفارشات</a></li>
                        <li><a href="{{ url('admin/discounts') }}">کد های تخفیف</a></li>
                    </ul>
                </li>

                <li id="li_3">
                    <a href="">
                        <span class="fa fa-comment"></span>
                        <span>{{ $comment }}</span>
                    </a>
                </li>


                <li id="li_6">
                    <a href="{{ url('admin/statistics') }}">
                        <span class="fa fa-area-chart"></span>
                        <span>آمار سایت</span>
                    </a>
                </li>

                <li id="li_7">
                    <a href="">
                        <span class="fa fa-user"></span>
                        <span>مدیریت کاربران</span>
                    </a>
                </li>

                <li id="li_4">
                    <a href="{{ url('admin/setting') }}">
                        <span class="fa fa-cogs"></span>
                        <span>تنظیمات</span>
                    </a>
                </li>


                <li id="li_5">


                </li>

            </ul>


        </div>



        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            @yield('content')

        </div>
    </div>


</div>


<script type="text/javascript" src="{{ url('js/jquery.js') }}"></script>
<script type="text/javascript" src="{{ url('js/js.js') }}"></script>

@yield('footer')
</body>


</html>

