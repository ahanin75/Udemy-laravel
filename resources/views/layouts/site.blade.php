
<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ url('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ url('css/bootstrap-rtl.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/site.css') }}" rel="stylesheet">
    <link href="{{ url('css/font-awesome.css') }}" rel="stylesheet">
    <title>{{ config('app.name') }}</title>
    @yield('header')
</head>
<body>



<div class="container-fluid" style="background:white;padding-bottom:20px">

    <div class="row line"></div>


    <div class="row" style="width:95%;margin:auto;">
        <div class="col-xs-12 col-sm-6 col-md-8">
            <div>
                <p class="logo_title">ایده پردازان جوان</p>
                <p>ارایه دهنده محتوای آموزشی در زمینه مهندسی برق و کامپیوتر</p>
            </div>
        </div>
        <div class="col-xs-6 col-md-4">

            <div style="margin-top:30px">


                @if(!Auth::check())

                    <a class="btn btn-warning" id="add_user" href="{{ url('login') }}">
                        <span class="fa fa-lock" ></span> @lang('messages.login')</a>

                    <a class="btn btn-success" id="register_user" href="{{ url('register') }}">
                        <span class="fa fa-user" ></span> {{ __('messages.register') }}</a>

                @endif





            </div>

        </div>
    </div>
</div>



<nav id="w0" class="navbar navbar-default" role="navigation">
    <div class="container"><div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w0-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span></button>
        </div>
        <div id="w0-collapse" class="collapse navbar-collapse">
            <ul id="w1" class="nav navbar-nav">
                <li class="active"><a href="{{ url('') }}">صفحه اصلی</a></li>
                @if(Auth::check())
                    <li><a href="{{ url('user') }}">پنل کاربری</a></li>
                @endif
                <li><a href="{{ url('category') }}">دسته بندی</a></li>
                <li><a href="">درباره ما</a></li>
                <li><a href="">تماس با ما</a></li>
                <li><a href="">مقررات سایت</a></li>
                @if(Auth::check())
                    <li><a href="{{ url('logout') }}">خروج</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>


<div class="container-fluid">



    @yield('content')


</div>






<script type="text/javascript" src="{{ url('js/bootstrap.js') }}"></script>
<script type="text/javascript" src="{{ url('js/jquery.js') }}"></script>
@yield('footer')
</body>
</html>

