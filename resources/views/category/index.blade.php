@extends('layouts/admin')

@section('header')
    <title>مدیریت دسته ها</title>
@endsection

@section('content')

    <div class="box_title">
        <span>مدیریت دسته ها</span>
    </div>



    <div>
        <a class="btn btn-success" href="{{ url('admin/category/create') }}">
            دسته جدید
        </a>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>ردیف</th>
                <th>نام دسته</th>
                <th>سر دسته</th>
                <th>عملیات</th>
            </tr>
            </thead>
            <?php $i=1; ?>
            @foreach($category as $key=>$value)

                <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $value->cat_name }}</td>
                    <td>
                        <?php
                        $parent=$value->parent
                        ?>
                        @if($parent)
                            {{$parent->cat_name }}
                            @else
                              -
                        @endif
                    </td>
                    <td>
                        <a style="color:#368bff" href="{{ url('admin/category').'/'.$value->id.'/edit' }}">
                            <span class="fa fa-edit"></span>
                        </a>
                        <a style="cursor:pointer;color:red;padding-right:5px" onclick="del_row('<?= $value->id ?>','<?= url('admin/category') ?>','<?= Session::token() ?>')">
                            <span class="fa fa-remove"></span>
                        </a>
                    </td>
                </tr>
                <?php $i++; ?>
            @endforeach
        </table>

        {{ $category->links() }}


    </div>
@endsection
