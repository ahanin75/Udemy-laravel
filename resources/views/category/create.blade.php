@extends('layouts/admin')


@section('header')
<title>افزودن دسته</title>
@endsection

@section('content')

    <div class="box_title">
        <span>افزودن دسته</span>
    </div>

    {!! Form::open(['url' => 'admin/category']) !!}
    <div class="form-group">
       {{  Form::label('cat_name', 'نام دسته : ') }}
       {{ Form::text('cat_name',null,['class' => 'form-control']) }}
        @if($errors->has('cat_name'))
        <span style="color:red;font-size:13px">{{ $errors->first('cat_name') }}</span>
        @endif
    </div>


    <div class="form-group">
        {{  Form::label('cat_ename', 'نام لاتین دسته : ') }}
        {{ Form::text('cat_ename',null,['class' => 'form-control']) }}
        @if($errors->has('cat_ename'))
            <span style="color:red;font-size:13px">{{ $errors->first('cat_ename') }}</span>
        @endif
    </div>

    <div class="form-group">
        {{  Form::label('cat_parent', 'انتخاب سر دسته : ') }}
        {{ Form::select('parent_id',$cat_list,null,['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{  Form::submit('ثبت',['class'=>'btn btn-success']) }}
    </div>
    {!! Form::close() !!}

@endsection