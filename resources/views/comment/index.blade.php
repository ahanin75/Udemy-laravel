@extends('layouts/admin')


@section('header')
    <title>مدیریت نظرات</title>
@endsection

@section('content')

    @if(sizeof($comment)==0)

        <p style="text-align:center;color: red;padding-top:40px;">هیچ نظری تا کنون ثبت نشده</p>

    @endif

    @foreach($comment as $key=>$value)



        <div id="comment_box_<?= $value->id ?>" style="width:95%;margin:15px auto;border:1px solid <?= ($value->status==1) ? '#F9F9F9' : 'red;' ?>">


            <div style="width:95%;margin:auto;padding-top:10px;padding-bottom:10px">
                <p style="color:red;"><span>ارسال شده توسط : </span>{{ $value->name }}
                    @if($value->parent_id!=0)

                        - <span>در پاسخ به : </span>
                    <?php
                        $parent=$value->get_parent;
                    ?>
                        {{ $parent->name }}
                    @endif
                </p>
                {!!   nl2br(strip_tags($value->content)) !!}

                <p style="padding-top:20px;color:red">ثبت شده در محصول</p>
                <p style="padding-top:5px;">
                    {{ $value->product->title }}
                </p>
                <div  id="answer_comment_form_<?= $value->id ?>" class="answer_comment_div">



                    {!! Form::open(['url' => 'admin/comment/create']) !!}
                    @if($value->parent_id==0)

                        <input type="hidden" value="{{ $value->id }}" id="parent_id" name="parent_id">

                    @else

                        <input type="hidden" value="{{ $parent->id }}" id="parent_id" name="parent_id">

                    @endif
                    <input type="hidden" value="{{ $value->product_id }}"  name="product_id">





                    <div class="form-group" style="margin-right:0px;">
                        {{ Form::textArea('content',null,['class' => 'form-control','style'=>'width:100%;height:200px;resize:none']) }}
                        @if($errors->has('content'))
                            <span style="color:red;font-size:13px">{{ $errors->first('content') }}</span>
                        @endif
                    </div>


                    <div class="form-group" style="margin-right:0px;">
                        {{  Form::submit('ثبت نظر',['class'=>'btn btn-success']) }}
                    </div>
                    {!! Form::close() !!}

                </div>

                <p style="padding-top: 20px;">

                <span id="status_text_<?= $value->id ?>" onclick="set_status(<?= $value->id ?>)" style="cursor:pointer;color:green">


                    @if($value->status==1)

                        تایید شده

                    @else

                        تایید نشده
                    @endif


                </span> -
                    <span onclick="add_answer(<?= $value->id ?>)" style="cursor:pointer;color:blue">ثبت پاسخ</span> -

                    <span style="cursor:pointer">
                     <a style="color:red" onclick="del_row('<?= $value->id ?>','<?= url('admin/comment') ?>','<?= Session::token() ?>')">حذف </a>
                </span>
                </p>
            </div>

        </div>

    @endforeach


    {{ $comment->links() }}

@endsection

@section('footer')
<script>
set_status=function (id)
{
    $.ajaxSetup(
        {
            'headers':{
                'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
            }
        });

    $.ajax({
        url:'<?= url('admin/set_status') ?>',
        type:'POST',
        data:'product_id='+id,
        success:function (data)
        {
            if(data==1)
            {
                $("#status_text_"+id).html('تایید شده');
                document.getElementById('comment_box_'+id).style.border='1px solid #F9F9F9';
            }
            else
            {
                $("#status_text_"+id).html('تایید نشده');
                document.getElementById('comment_box_'+id).style.border='1px solid red';
            }
        }
    });
};
add_answer=function (id)
{
    $("#answer_comment_form_"+id).show();
}
</script>
@endsection