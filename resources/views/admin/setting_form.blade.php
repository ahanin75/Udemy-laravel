@extends('layouts/admin')



@section('header')
    <title>تنظیمات پرداخت</title>
@endsection

@section('content')


    <div class="box_title">
        <span>تنظیمات پرداخت</span>
    </div>


    {!! Form::open(['url' => 'admin/setting']) !!}
    <div class="form-group">
        {{  Form::label('terminalid', 'ترمینال ای دی : ') }}
        {{ Form::text('terminalid',$data['terminalid'],['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{  Form::label('username', 'نام کاربری : ') }}
        {{ Form::text('username',$data['username'],['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{  Form::label('password', 'کلمه عبور : ') }}
        {{ Form::text('password',$data['password'],['class' => 'form-control']) }}
    </div>

    <div class="form-group">
        {{  Form::submit('ثبت',['class'=>'btn btn-success']) }}
    </div>
    {!! Form::close() !!}
@endsection