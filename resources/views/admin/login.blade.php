<html lang="fa">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ url('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ url('css/bootstrap-rtl.min.css') }}" rel="stylesheet">
    <link href="{{ url('css/admin.css') }}" rel="stylesheet">
    <title>ورود به پنل مدیریت</title>

</head>

<body>








<div class="container-fluid">

    <div class="row" style="width:45%;margin:10px auto;background:white">


        <div class="box_title">
            <span>ورود به بخش مدیریت</span>
        </div>

        <form id="register_form" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}

            <div class="form-group">
                <label class="control-label" for="users-fname">نام کاربری</label>
                <input id="name" type="text" class="form-control" name="username" value="{{ old('username') }}">
                <div class="help-block">
                    @if ($errors->has('username'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label class="control-label" for="users-fname">کلمه عبور</label>
                <input id="name" type="password" class="form-control" name="password" >
                <div class="help-block">
                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>


            <div class="form-group">
                <label class="control-label" for="users-fname">تصویر امنیتی</label>
                <img src="{{ url('captcha') }}" style="margin-right:45px">
            </div>


            <div class="form-group">
                <label class="control-label" for="users-fname"></label>
                <input id="name" type="text" class="form-control" name="captcha" >
                <div class="help-block">
                    @if ($errors->has('captcha'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('captcha') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">

                <label class="control-label" for="users-fname">
                    مرا به خاطر بسپار</label>
                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>


            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">ورود به سایت</button>
            </div>
        </form>



    </div>

</div>


</body>


</html>






