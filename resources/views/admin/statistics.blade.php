@extends('layouts/admin')



@section('header')
    <title>آمار بازدید سایت</title>
@endsection

@section('content')


    <div id="container" style="direction:ltr">

    </div>

@endsection

<?php

        $d='';
        $v='';
        $v_t='';
        foreach ($date as $key=>$value)
        {
            $d.="'$value',";
        }
        foreach ($view as $key=>$value)
        {
           $v.="$value,";
        }
        foreach ($total_view as $key=>$value)
        {
            $v_t.="$value,";
        }


?>

@section('footer')
<script type="text/javascript" src="{{ url('js/highcharts.js') }}"></script>
<script>


    Highcharts.chart('container', {

        title: {
            text: 'آمار بازید 30 روز اخیر'
        },

        subtitle: {
            text: ''
        },
        chart:{

            style:{
                fontFamily:'IRANSansWeb'
            }
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        xAxis: {
            categories:[<?= $d ?>]
        },
        legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'top',
            y:30
        },


        series: [{
            name: 'آمار بازدید یکتا',
            data: [<?= $v ?>]
        }, {
            name: 'آمار بازدید کل',
            data: [<?= $v_t ?>],
            marker: {
                symbol: 'circle'
            },
            color:'red'
        }]

    });
</script>

@endsection