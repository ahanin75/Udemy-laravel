@if(Session::has('cart'))

    <?php

    $cart_price=0;
    $total_price=0;

    ?>

    <div id="tbl_cart">
        <table id="cart_tbl" class="table table-bordered" style="margin: 30px auto;width:90%;">

            <tr>
                <th>ردیف</th>
                <th>عنوان محصول</th>
                <th>تعداد</th>
                <th>هزینه</th>
                <th>حذف</th>
            </tr>
            <?php $i=1; ?>
            @foreach(Session::get('cart') as $key=>$value)

                <?php
                $product=\App\Product::findOrFail($key);
                $price=$value*$product->price;
                $total_price+=$price;
                $cart_price+=$price;
                ?>
                <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $product->title }}</td>
                    <td style="width:10%">

                        <input id="cart_input_{{ $product->id }}" class="form-control" style="width:95%;margin:auto;text-align:center" value="{{ $value }}">
                        <span style="padding-top:4px;color:sandybrown;cursor: pointer;" onclick="set_number_order('{{ $product->id }}')" class="fa fa-refresh"></span>
                    </td>

                    <td>{{ number_format($price) }}</td>
                    <td>
                        <span class="fa fa-remove" style="color:red;cursor:pointer" onclick="del_order('{{ $product->id }}')"></span>
                    </td>
                </tr>
                <?php $i++; ?>
            @endforeach

        </table>




        <?php

        Session::put('total_price',$total_price);
        if(Session::has('discount'))
        {
            $cart_price=$cart_price-((Session::get('discount')*$cart_price)/100);
            Session::put('cart_price',$cart_price);
        }
        else
        {
            Session::put('cart_price',$cart_price);
        }

        ?>



        <div style="width:90%;margin:auto">
            <p><span>هزینه کل : </span>  <span>{{ number_format($total_price) }} ریال</span> </p>

            <p><span>هزینه قابل واریز : </span> <span>{{ number_format($cart_price) }} ریال</span></p>




            <div class="box_title">

                <div class="discount_div_1">

                    <p>در صورت داشتین کد تخفیف آن را وارد نمایید</p>

                </div>

                <div  class="discount_div_2">
                    <input type="text" name="discount" id="discount" class="form-control" style="width:100%"></div>

                <div class="discount_div_3">
                    <button onclick="send_discount()" class="btn btn-success">ارسال</button>
                    <span id="discount_msg" style="color:red;font-size:13px;">@isset($error) {{ $error }} @endisset</span>
                </div>

            </div>


        </div>



        <div style="width:90%;margin:50px auto">

            @if(Auth::check())



                <form id="register_form" style="margin-right:0px" method="POST" action="{{ url('user/add_order') }}">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label class="control-label" style="width:110px" for="users-fname">نام : </label>
                        <input  type="text" class="form-control" name="fname" value="@if(old('fname')){{ old('fname') }}@else{{ Auth::user()->fname }}@endif">
                        <div class="help-block">
                            @if ($errors->has('fname'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('fname') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label" style="width:110px" for="users-fname">نام خانوادگی : </label>
                        <input id="lname" type="text" class="form-control" name="lname" value="@if(old('lname')){{ old('lname') }}@else{{ Auth::user()->lname }}@endif">
                        <div class="help-block">
                            @if ($errors->has('lname'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('lname') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" style="width:110px" for="users-fname">ایمیل : </label>
                        <input id="email" type="text" class="form-control" name="email" value="@if(old('email')){{ old('email') }}@else{{ Auth::user()->email }}@endif">
                        <div class="help-block">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label" style="width:110px" for="users-fname">شماره موبایل : </label>
                        <input id="mobile" type="text" class="form-control" name="mobile" value="{{ old('mobile') }}">
                        <div class="help-block">
                            @if ($errors->has('mobile'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <p style="color:red">در صورت تمایل به درخواست پستی می توانید این قسمت را تکمیل نمایید</p>
                    <div class="form-group">
                        <label class="control-label" style="width:110px" for="users-fname">کد پستی : </label>
                        <input name="zip_code" type="text" class="form-control"  value="{{ old('zip_code') }}">
                        <div class="help-block">
                            @if ($errors->has('zip_code'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('zip_code') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="control-label" style="width:110px" for="users-fname">آدرس پستی : </label>
                        <textarea id="address_order" name="address">{{ old('address') }}</textarea>
                    </div>


                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">ثبت سفارش</button>
                    </div>
                </form>

            @else

                <p style="text-align:center"><a href="{{ url('login') }}">جهت ثبت سفارش باید وارد سایت شوید</a></p>

            @endif

        </div>


    </div>



@else


    <p style="padding-top:30px;padding-bottom:20px;text-align:center;color:red;">سبد خرید شما خالی می باشد</p>

@endif