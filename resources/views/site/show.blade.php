@extends('layouts/site')


@section('content')
    <div class="row" style="width:95%;margin:auto;box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 2px 0px;background:white">

        <div class="col-xs-6">

            <div class="show_img_box">
                @if(!empty($product->img))
                    <a href=""><img src="{{ url($product->img) }}"></a>
                @endif
            </div>

        </div>





        <div class="col-xs-6">

            <div class="show_title_box">{{ $product->title }}</div>

            <ul class="list-unstyled">

                <li><span class="fa fa-user"></span> مدرس : علی صدیقی</li>

                <li><span class="fa fa-file-o"></span> <span>تعداد قسمت های دوره : </span> <span> قسمت{{ $product->download_number }}</span></li>

                <li><span class="fa fa-clock-o"></span> <span>مدت زمان دوره : </span> <span> {{ $product->download_tiems }}</span></li>

                <li><span class="fa fa-file-zip-o"></span> <span>حجم دوره :</span> <span>{{ $product->download_size }}</span></li>

                <li><span class="fa fa-shopping-cart"></span> <span>هزینه دوره : </span> <span>

                    <?php

                        if(!empty($product->price))
                        {
                            echo number_format($product->price).' ریال';
                        }
                        else
                        {
                            echo 'رایگان';
                        }

                        ?>

                </span></li>




            </ul>



            @if(!empty($product->price))
                <form method="post" action="{{ url('cart') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="product_id" value="<?= $product->id ?>">
                    <button type="submit" class="btn btn-success" style="margin-top:20px" >افزودن به سبد خرید</button>

                </form>
             @endif



        </div>



        <div style="width:90%;float:right;margin-right:5%;margin-top:30px;">

            {!! $product->text !!}
        </div>


        <div style="clear:both"></div>
        <div class="box_title"  style="margin-top:40px">
            <span>ارسال نظر</span>
        </div>


        <div style="width:95%;margin:auto" id="add_comment_form">

            <p id="msg_tag" style="color:red;padding-bottom:10px;">

                {{ Session::get('msg') }}
            </p>

            {!! Form::open(['url' => 'add_comment']) !!}
            <input type="hidden" value="0" id="parent_id" name="parent_id">
            <input type="hidden" value="{{ $product->id }}"  name="product_id">
            <div class="form-group">
                {{  Form::label('name', 'نام و نام خانوادگی : ') }}
                {{ Form::text('name',null,['class' => 'form-control']) }}
                @if($errors->has('name'))
                    <span style="color:red;font-size:13px">{{ $errors->first('name') }}</span>
                @endif
            </div>

            <div class="form-group">
                {{  Form::label('email', 'ایمیل : ') }}
                {{ Form::text('email',null,['class' => 'form-control']) }}
                @if($errors->has('email'))
                    <span style="color:red;font-size:13px">{{ $errors->first('email') }}</span>
                @endif
            </div>


            <div class="form-group">
                {{ Form::textArea('content',null,['class' => 'form-control','style'=>'width:75%;height:200px;resize:none']) }}
                @if($errors->has('content'))
                    <span style="color:red;font-size:13px">{{ $errors->first('content') }}</span>
                @endif
            </div>


            <div class="form-group">
                {{  Form::submit('ثبت نظر',['class'=>'btn btn-success']) }}
            </div>
            {!! Form::close() !!}


        </div>

        <div style="clear:both"></div>
        <div class="box_title"  style="margin-top:40px">
            <span>نظرات ثبت شده</span>
        </div>

        @foreach($comment as $key=>$value)

            <div class="comment_box">


                <div class="comment_title">
                         <span style="float:right;padding-right:10px;padding-top:10px;">
                نوشته شده توسط  : {{ $value->name }}
                </span>

                    <span style="float:left;padding-left:10px;padding-top:10px;">

                <span>تاریخ ثبت نظر : </span>

                        {{ $date = jdate($value->created_at)->format('%d-%m-%Y') }}
                </span>
                    <div style="clear:both"></div>
                </div>





            <div style="width:98%;margin:auto">

                <div style="padding-right:10px;padding-top:20px;padding-bottom: 20px;">
                   {!!   nl2br(strip_tags($value->content)) !!}
                </div>

            </div>



                @foreach($value->get_child as $key2=>$value2)


                    <div class="comment_parent_title">

                    <span style="float:right;padding-right:10px;padding-top:10px;">
                    نوشته شده توسط : {{ $value2->name }}
                    </span>

                        <span style="float:left;padding-left:10px;padding-top:10px;">

                        <span>تاریخ ثبت نظر : </span>
                            {{ $date = jdate($value2->created_at)->format('%d-%m-%Y') }}
                    </span>
                        <div style="clear:both"></div>
                    </div>
                    <div style="width:98%;margin:auto">

                        <div style="padding-right:10px;padding-top:20px;padding-bottom: 20px;">
                            {!!   nl2br(strip_tags($value2->content)) !!}
                        </div>

                    </div>

                @endforeach

             </div>


            <p style="color:red;width:95%;margin:auto;cursor:pointer" onclick="add_answer(<?= $value->id ?>,'{{ $value->name }}')">ارسال پاسخ</p>
        @endforeach

        <div style="width:95%;margin:auto">
            {{ $comment->links() }}
        </div>

        @if(sizeof($comment)==0)
            <p style="color:red;padding-top:20px;padding-bottom:20px;text-align:center">
                تاکنون نظری برای این محصول ثبت نشده
            </p>
        @endif


    </div>
@endsection

@section('footer')
<script>
add_answer=function (id,name)
{
    document.getElementById('parent_id').value=id;
    var msg= 'ارسال پاسخ به : ' + name;
    $("#msg_tag").html(msg);
    window.location='#add_comment_form';
}
</script>
@endsection