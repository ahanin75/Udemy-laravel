@extends('layouts/site')

@section('content')

    <div class="row" style="width:90%;margin:10px auto;background:white;padding-bottom: 20px;">


        @foreach($category as $key=>$value)

            <div style="width:90%;margin:20px auto;">


                <p style="border-bottom:1px solid #EEEFF1;padding-top:10px;padding-bottom:10px">
                    <span class="fa fa-reorder" style="padding-left: 10px;"></span>
                    <a style="color:black;"  href="{{ url('category').'/'.$value->cat_ename }}"><?= $value->cat_name ?></a></p>


                <ul class="list-unstyled">

                    @foreach($value->getChild as $key2=>$value2)


                        <li>

                            <span><a style="color:black;" href="{{ url('category').'/'.$value->cat_ename.'/'.$value2->cat_ename }}"><?= $value2->cat_name ?></a></span>
                        </li>

                    @endforeach
                </ul>
            </div>
        @endforeach
    </div>
@endsection