@extends('layouts/site')

@section('content')


    @foreach($product as $key=>$value)

        <div class="product_box">

            <div class="img_box">
                
                @if(!empty($value->img))
                    
                    <img src="{{ url($value->img) }}">
                @endif    
                
            </div>

            <div class="product_title">
               <a href="{{ url('').'/'.$value->title_url }}"> {{ $value->title }}</a>
            </div>

            <div class="product_footer">
                <div class="right"> مدرس : علی صدیقی</div>
                <div class="left">
                    @if($value->price==0)
                        رایگان
                    @else
{{ number_format($value->price) }} ریال
                    @endif
                </div>
            </div>

        </div>

    @endforeach

   <div style="width:100%;float:right">
       {{ $product->links() }}
   </div>

@endsection