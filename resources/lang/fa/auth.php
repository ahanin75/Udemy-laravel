<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'کاربری با اطلاعات وارد شده یافت نشد',
    'throttle' => 'به دلیل تعداد بالای درخواست ناموفق :seconds ثانیه امکان لاگین را ندارید',

];
